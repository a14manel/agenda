<?php
session_start ();
include 'connect.php';

//echo print_r($_SESSION ["search"]);
?>
<!DOCTYPE html>
<html>
<head>
<title>Inici</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="stylesheet" type="text/css" href="./css/llista.css" />
</head>
<body>
	<header>
	<img id="imgAgenda" src="./img/agenda.png" height="50" width="50"><span>Agenda de contactes</span>
	</header>
	<h1>Llistat de contactes</h1>
	<div id="cos">
		<div id="barraSuperior">	
			<form id="formAfegirContacte" action="crear_modificar.php" method="post">
			<button id="btnAfegirContacte" onmouseover="pintar()" onmouseout="despintar()">
				<input type="hidden" id="create" name="create" value="true">
				<img src="./img/afegirContacte.png" alt="afegir_contacte"></button>
			</form>
			<div id="divBtnLogout">
				<button type="button" name="btnLogout" id="btnLogout" onclick="accioLogout()"><img src="./img/logout.png" alt="logout"></button>
			</div>
		</div>
		<div id="barraCerca">
			<form action="cerca.php" method="post">
			Cerca  <input type="text" name="txtCampCerca">
			<input type="radio" name="campCerca" value="checkNom" checked="checked">Nom
			<input type="radio" name="campCerca" value="checkCognom">Cognom
			<input type="image" src="./img/cerca.png" width="25" height="25">
			</form>
		</div>
	<?php
	// Creem l'objecte que estableix la connexió
	//if (!strcmp($_SESSION ["conteDades"],"null") || empty($_SESSION ["search"])) {
	if (empty($_SESSION ["search"])) {	
	$conn = getConection ();
	// Fem el select dels contactes contra la base de dades i ho executem
	$sql = "SELECT * FROM contacts where ID_USER = " . $_SESSION ["id"] . ";";
	$result = mysqli_query ( $conn, $sql );
	
	if (mysqli_num_rows ( $result ) > 0) { ?>
		<!--Construcció de la capçalera de la taula que mostrarà els contactes-->		
		<div id="divTable"><table><tr>
		<th>Nom</th><th>Cognom</th><th>Correu</th>
		<th>Telefon</th><th>Data de Naixement</th>
		<th>Editar</th><th>Esborrar</th>
		</tr>
	<?php
		$num_rows = 0;
		while ( $row = mysqli_fetch_assoc ( $result ) ) { ?>
			<tr>
			<td> <?php echo $row ["NAME"]; ?> </td>
			<td> <?php echo $row ["LAST_NAME"]; ?> </td>
			<td> <?php echo $row ["MAIL"]; ?> </td>
			<td> <?php echo $row ["PHONE"]; ?> </td>
			<td> <?php echo $row ["BIRTH_DATE"]; ?> </td>			
			
			<input id="option" type="hidden" name="option" value="nothing">
			<!--Per modificar creem un formulari que enviï les dades a crear_modificar.php perquè l'usuari pugui editar-->			
			<form method="post" id= <?php echo "formContacte" . ++$num_rows ?> action="crear_modificar.php">						
				<td  onclick="confirmEdit(<?php echo $num_rows; ?>)">
					<input type="image" name="submitEdit" value="edit" alt="edit" src="./img/pen.png" height="35" width="35">
				</td>
				<input id="id_contact" type="hidden" name="id_contact" value= <?php echo $row ["ID_CONTACT"]; ?>  >			
				<input type="hidden" id="option" name="option" value="edit">
			
			</form>
			<!--Per eliminar fem servir directament l'arxiu auxiliar delete.php, que eliminarà el contacte i ens redirigirà aqui mateix (llista.php)-->
				<form method="post" id= <?php echo "formContacte" . ++$num_rows ?> action="delete.php">			
				<td onclick="confirmDelete(<?php echo $num_rows; ?>)">
					<input type="image" name="submitDelete" value="delete" alt="delete" src="./img/delete.png" height="35" width="35">
				</td>
				<input id="id_contact" type="hidden" name="id_contact" value= <?php echo $row ["ID_CONTACT"]; ?>  >
				<input id="option" type="hidden" name="option" value="delete">			
			</form>			
			</tr>
		 	<?php
		}		
		echo '</table>';
		echo '</div>';
	} else {
		 //"0 results";
        if(empty($_SESSION["id"])){
            header("Location: login.php");
            exit();
        }
        else{
            header("Location: crear_modificar.php");
            exit();
        }
	}
	
	// Tancament de la connexió amb la Base de Dades
	mysqli_close ( $conn );
	
	
	}
	else { //S'ha realitzat una cerca i la taula s'omple amb els resultats d'aquesta --> $_SESSION ["search"]
		?>		
		<!--Construcció de la capçalera de la taula que mostrarà els contactes-->		
		<div id="divTable"><table><tr>
		<th>Nom</th><th>Cognom</th><th>Correu</th>
		<th>Telefon</th><th>Data de Naixement</th>
		<th>Editar</th><th>Esborrar</th>
		</tr>
		<?php		
		$num_rows = 0;
		foreach ( $_SESSION ["search"] as $key => $row ) { ?>
			<tr>
			<td> <?php echo $row ["NAME"]; ?> </td>
			<td> <?php echo $row ["LAST_NAME"]; ?> </td>
			<td> <?php echo $row ["MAIL"]; ?> </td>
			<td> <?php echo $row ["PHONE"]; ?> </td>
			<td> <?php echo $row ["BIRTH_DATE"]; ?> </td>			
			
			<input id="option" type="hidden" name="option" value="nothing">
			<!--Per modificar creem un formulari que enviï les dades a crear_modificar.php perquè l'usuari pugui editar-->			
			<form method="post" id= <?php echo "formContacte" . ++$num_rows ?> action="crear_modificar.php">						
				<td  onclick="confirmEdit(<?php echo $num_rows; ?>)">
					<input type="image" name="submitEdit" value="edit" alt="edit" src="./img/pen.png" height="35" width="35">
				</td>
				<input id="id_contact" type="hidden" name="id_contact" value= <?php echo $row ["ID_CONTACT"]; ?>  >			
				<input type="hidden" id="option" name="option" value="edit">
			
			</form>
			<!--Per eliminar fem servir directament l'arxiu auxiliar delete.php, que eliminarà el contacte i ens redirigirà aqui mateix (llista.php)-->
				<form method="post" id= <?php echo "formContacte" . ++$num_rows ?> action="delete.php">			
				<td onclick="confirmDelete(<?php echo $num_rows; ?>)">
					<input type="image" name="submitDelete" value="delete" alt="delete" src="./img/delete.png" height="35" width="35">
				</td>
				<input id="id_contact" type="hidden" name="id_contact" value= <?php echo $row ["ID_CONTACT"]; ?>  >
				<input id="option" type="hidden" name="option" value="delete">			
			</form>			
			</tr>
			
		 	<?php
		}
		unset($_SESSION ["search"]);
		?>
		</table>
			</div>
			
		<?php
	}
	?>
	
	<footer>
	Copyright © 2016 Manel Barba, Adán Sevilla
	</footer>
	<?php
	if (!strcmp($_SESSION ["conteDades"],"null")) {
			echo '<script language="javascript">';
			echo 'alert("No hi ha resultats que coincideixin amb la cerca")';
			echo '</script>';
			$_SESSION ["conteDades"] = "-1";
	}
	?>
	<script type="text/javascript" src="./js/llista.js"> </script>
</body>
</html>