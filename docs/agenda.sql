CREATE DATABASE agenda;

USE agenda;

CREATE TABLE users (
     ID_USER INT NOT NULL AUTO_INCREMENT,
	 USER VARCHAR(35) NOT NULL,
     PASSWORD VARCHAR(12) NOT NULL,
     PRIMARY KEY (ID_USER)
    );

CREATE TABLE contacts (
     ID_CONTACT INT NOT NULL AUTO_INCREMENT,
	 NAME VARCHAR(35) NOT NULL,
	 LAST_NAME VARCHAR(35) NOT NULL,
	 MAIL VARCHAR(35) NOT NULL,
     PHONE VARCHAR(9) NOT NULL,
	 BIRTH_DATE VARCHAR(10) NOT NULL,
	 ID_USER  INT NOT NULL,
     PRIMARY KEY (ID_CONTACT),
	 FOREIGN KEY (ID_USER) REFERENCES users(ID_USER)
);

insert into users (USER,PASSWORD) values ("manbar","manbar");
insert into users (USER,PASSWORD) values ("adasev","adasev");

 insert into contacts (NAME,LAST_NAME,MAIL,PHONE,BIRTH_DATE,ID_USER) values ('Manel','Barba','Manel@mail.com',123456789,'12/05/1984',1);
 insert into contacts (NAME,LAST_NAME,MAIL,PHONE,BIRTH_DATE,ID_USER) values ('Jaina','Proudmore','Jaina@mail.com',987654321,'12/05/1984',1);
 insert into contacts (NAME,LAST_NAME,MAIL,PHONE,BIRTH_DATE,ID_USER) values ('Varian','Wrynn','Varian@mail.com',123789654,'12/05/1984',1);
 insert into contacts (NAME,LAST_NAME,MAIL,PHONE,BIRTH_DATE,ID_USER) values ('Genn','Cringris','Genn@mail.com',456127893,'12/05/1984',1);

 insert into contacts (NAME,LAST_NAME,MAIL,PHONE,BIRTH_DATE,ID_USER) values ('Subaru','Natsuki','Subaru@mail.com',963852741,'12/05/1984',2);
 insert into contacts (NAME,LAST_NAME,MAIL,PHONE,BIRTH_DATE,ID_USER) values ('Stephanie','Dola','Stephanie@mail.com',369258147,'12/05/1984',2);
 insert into contacts (NAME,LAST_NAME,MAIL,PHONE,BIRTH_DATE,ID_USER) values ('Izuna','Hatsune','Izuna@mail.com',147896325,'12/05/1984',2);
 insert into contacts (NAME,LAST_NAME,MAIL,PHONE,BIRTH_DATE,ID_USER) values ('Araragi','Koyomi','Araragi@mail.com',149685237,'12/05/1984',2);

