# README #

Aquest document et donarà els primers pasos necessaris per posar el projecte en funcionament.  


* Estructura
>La estructura del projecte està organitzada en carpetes i una sèrie de PHP/HTML a l'arrel.
>> ### Docs ###
>>>Trobarem la Guia d'estil i el script de creació de la BD
>> ### CSS ###
>>>Trobarem els diferents fulls d'estil.
>> ### IMG ###
>>>Trobarem els arxius d'imatges en format PNG de la web.
>> ### JS ###
>>>Trobarem els arxius javascript de la pàgina.
>> ### Arrel ###
>>>Trobarem la pagina inicial i els diferents php y html.

* Creació de la base de dades.

> Primer necessitarem un MysqlServer amb una BD anomenada agenda creada.
<introduïm la següent comanda en un terminal per crear i introduir dades.
>> mysql -u root -p < /ruta del sql/agenda.sql  
Disposem del usuari manbar amb password manbar i l'usuari adasev amb password adasev per fer les proves

* Manuel Barba i Adan Sevilla