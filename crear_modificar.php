<?php
session_start ();
include 'connect.php';

if (!strcmp($_POST["option"],"edit")) {
	//echo "Es procedeix a llegir el contacte...";	
	$conn = getConection();
	//coge bien la id del usuario de session y el contacto con el nombre del campo adecuado.<?php

	$sql = "SELECT * FROM contacts WHERE ID_USER = ".$_SESSION["id"]." AND ID_CONTACT = ".$_POST["id_contact"].";";
	
	echo $sql;
	
	$result = mysqli_query ( $conn, $sql );
	
	if (mysqli_num_rows ( $result ) > 0) { 
		$row = mysqli_fetch_assoc ( $result );
		}
	mysqli_close($conn);	
}

else {
	//echo "crear";
}

?>
<!DOCTYPE html>
<html>
<head>
<title>Crear / Editar</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="stylesheet" type="text/css" href="./css/crear_modificar.css"/>

</head>

<body>
	<header>
	<img id="imgAgenda" src="./img/agenda.png" height="50" width="50"><span >Crear / Editar</span>
	</header>
	<div id="cos">	
		<div id="divBtnLogout">
			<button type="button" name="btnLogout" id="btnLogout" onclick="accioLogout()"><img src="./img/logout.png" alt="logout"></button>
		</div>
			<!-- delete.php inserta, actualiza y borra los contactos		-->
		<form id="formContacte" action="delete.php" method="post" onsubmit="return validateForm()">
			<div class="camp">Nom<input type="text" name="txtNom" value=<?php echo $row ["NAME"]; ?> >
			</div>
			<div class="camp">Cognom<input type="text" name="txtCognom" value=<?php echo $row ["LAST_NAME"]; ?> >
			</div>			
			<div class="camp">Telèfon<input type="text" name="txtTelefon" value=<?php echo $row ["PHONE"]; ?> >
			</div>
			<div class="camp">Email<input type="text" name="txtEmail" value=<?php echo $row ["MAIL"]; ?> >
			</div>
			<div class="camp">Data de Naixement<input type="text" value="<?php if (!strcmp($_POST['option'],'edit')){echo $row ['BIRTH_DATE']; } else {echo " -- / -- / ---- ";}?>" name="txtDataNaixement" id="txtDataNaixement">
				<button type="button" id="btnDesplegar" onclick="desplegarCalendari()">Desplegar</button>		
			</div>		
			<div id="calendari"  width="100%" height="100%">
				<iframe src="iframeCalendari.html" name="iframeCalendari" id="iframeCalendari"></iframe>
			</div>	
			<div id="divButtons">
				<button type="submit">OK</button>
				<button type="reset">Cancelar</button>
			</div>
			<?php
				if (!strcmp($_POST["create"],"true")) {
					echo "<input type='hidden' id='create' name='create' value='true'>";
				}
				else {
					echo "<input type='hidden' id='create' name='create' value='false'>";				
				}
				
				if (!strcmp($_POST["option"],"edit")) {
					echo "<input type='hidden' id='option' name='option' value='edit'>";
					echo "<input id='id_contact' type='hidden' name='id_contact' value= ".$_POST["id_contact"].">";
				}
			?>
		</form>
	</div>
	<footer>
	Copyright © 2016 Bla bla
	</footer>
	<script type="text/javascript" src="./js/crear_modificar.js"> </script>
	<script type="text/javascript">
		function validateForm() {
		//La funció de validació dels camps del formulari ha estat col·locada aqui perquè no funcionava des del fitxer extern crear_modificar.js
		var campNom = document.forms["formContacte"]["txtNom"].value;
		var campCognom = document.forms["formContacte"]["txtCognom"].value;
		var campEmail = document.forms["formContacte"]["txtEmail"].value;
		var campTelefon = document.forms["formContacte"]["txtTelefon"].value;
		var campDataNaixement = document.forms["formContacte"]["txtDataNaixement"].value;
    	
    	if (campNom == "" || campCognom == "" || campEmail == "" || campTelefon == "" || campDataNaixement == "") {
      	alert("S'han d'omplir tots els camps del formulari");
      	return false;
    	}
    	
    	function validarLongitud (camp, longitud, nomCamp, minMax) {
			//El límit establert és el màxim
			if (minMax == 1) {			
				if (camp.length > longitud) {
					alert(nomCamp + " és massa llarg");
					return false;			
				}
			}
			else { //El límit establert és el mínim
				if (camp.length < longitud) {
					alert(nomCamp + " és massa curt");
					return false;
				}
			}
    	}
    	
    	function validarXifres (xifra) {
    		if(!xifra.match(/^\d+$/)) {
      		alert("Els dies, el mes i l'any només poden contenir només nombres");
     		return false;
			}
    	}
    	function validarBarra (barra) {
    		if (barra != "/") {
        		alert("Format de data incorrecte! Utilitza el format (dd/mm/aaaa)");
        	}
    	}
    	//VALIDACIÓ DE LA LONG DEL NOM
    	validarLongitud(campNom, 35, "El nom", 1);
    	validarLongitud(campNom, 3, "El nom", 0);
    	
    	//VALIDACIÓ DE LA LONG DEL COGNOM
    	validarLongitud(campCognom, 35, "El cognom", 1);
    	validarLongitud(campCognom, 3, "El cognom", 0);
    	
    	//VALIDACIÓ LONG DEL EMAIL
    	validarLongitud(campEmail, 35, "El email", 1);
    	
    	//VALIDACIÓ DATA NAIXEMENT
    	validarLongitud(campDataNaixement, 10, "La data de naixement", 1);
    	validarLongitud(campDataNaixement, 10, "La data de naixement", 0);
    	
    	var dies = x.substring(0,2);
    	var barra1 = x.substring(2,3);
    	var mes = x.substring(3,5);
    	var barra2 = x.substring(5,6);
    	var any = x.substring(6,10);
    
    	validarXifres(dies);
    	validarXifres(mes);
    	validarXifres(any);
    	validarBarra(barra1);
    	validarBarra(barra2);
    
    	//VALIDACIÓ Nº TELÈFON
    	validarLongitud(campTelefon, 9, "El número de telèfon", 1);
    	validarLongitud(campTelefon, 9, "El número de telèfon", 0);
    	
    	if(!campTelefon.match(/^\d+$/)) {
        alert("El telèfon no pot contenir caràcters no numèrics");
        return false;
		}
    			
		return true;
		}
	</script>
</body>
</html>

