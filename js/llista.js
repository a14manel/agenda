function confirmDelete(numeroContacte) {
	var r = confirm("Segur que vols esborrar!");
	if (r == true) {
		document.getElementById("option").value = "delete";
		document.getElementById("formContacte"+numeroContacte).submit();
	}else{
		document.getElementById("option").value = "nothing";
		//location.reload();
	}
}
function confirmEdit(numeroContacte) {
		var r = confirm("Vols editar el contacte ?");
		if (r == true) {
			document.getElementById("option").value = "edit";
			document.getElementById("formContacte"+numeroContacte).submit();
		}
		else{
			document.getElementById("option").value = "nothing";
			//location.reload();
	}
}

function accioLogout () {
	window.location.href = "logout.php";
}
function pintar() {
	document.getElementById("btnAfegirContacte").style.backgroundColor = "white";
}
function despintar() {
	document.getElementById("btnAfegirContacte").style.backgroundColor = "#3D6BB9";
}
